import { APOD_GET, setAPOD } from '../modules/APODModule';
import { getDaySubtractedFromToday } from '../lib/utils';
import axios from 'axios';

let dispatch = null;
let getState = null;

const APODMiddleware = store => {
  dispatch = store.dispatch;
  getState = store.getState;

  return next => action => {
    switch (action.type) {
    case APOD_GET:
      fetchAPOD(action.unit);
      break;
    default:
      break;
    }

    return next(action);
  };
};

function daysDataExists(unit) {
  const { APODS = [] } = getState();
  if (APODS.filter(APOD => APOD.date === getDaySubtractedFromToday(unit)).length) {
    return true;
  }
  return false;
}

function fetchAPOD(unit = 0) {
  if (daysDataExists(unit)) {
    return;
  }

  const key = "4AkGd9fJ6YgvvO2mJBNG3Q9Oym4xxUWROfpqvUZq";
  const url = `https://api.nasa.gov/planetary/apod?api_key=${key}&date=${getDaySubtractedFromToday(unit)}`;
  return axios.get(url)
    .then(({ data = {} }) => {
      dispatch(setAPOD(data));
    })
    .catch(err => {
      throw err;
    })
}

export default APODMiddleware;
