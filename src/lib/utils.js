import moment from 'moment';

export function getDaySubtractedFromToday(unit = 0) {
  return moment().subtract(unit, 'day').format("YYYY-MM-DD");
}