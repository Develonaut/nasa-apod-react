import { combineReducers } from 'redux'
import APODS from '../modules/APODModule';

const reducers = combineReducers({
    APODS,
})

export default reducers;