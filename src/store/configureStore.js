import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import rootReducer from './reducers';

import APODMiddleware from '../middleware/APODMiddleware';

const middleware = applyMiddleware(
  APODMiddleware,
);

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers = (process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
const enhancers = composeEnhancers(middleware);

export default () => {
  let store = createStore(persistedReducer, enhancers);
  let persistor = persistStore(store)
  return { store, persistor }
}