import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAPOD, getAPODS } from '../modules/APODModule';
import { getDaySubtractedFromToday } from '../lib/utils';

class PhotoOfTheDay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    }
  }

  componentDidMount() {
    const { getAPOD: dispatchGetAPOD } = this.props;
    dispatchGetAPOD(0);
  }

  handleClick = ({ target: { id }}) => {
    const { getAPOD: dispatchGetAPOD } = this.props;
    const { index } = this.state;
    const _index = (id === "prev") ? index + 1 : index - 1;
    this.setState({ index: _index });
    dispatchGetAPOD(_index);
  }

  getSelectedAPOD = () => {
    const { index } = this.state;
    const { APODS = [] } = this.props;
    return APODS.filter(APOD => APOD.date === getDaySubtractedFromToday(index))[0] || {};
  }

  render() {
    const { index } = this.state;
    const { APODS } = this.props;
    return (
      <div className="App">
        <ul>
          { 
            APODS.map(({ title, url, date }) => {
              const selectedDate = getDaySubtractedFromToday(index);
              const imgClass = (date === selectedDate) ? "apod" : "apod hidden";
              return (
                <li className={imgClass} key={date}>
                  <div className="img" style={{ backgroundImage: `url(${url})`}}/>
                  <h1 style={{ color: "white" }}>{title}</h1>
                </li>
              );
            })
          }
        </ul>
        <div style={{ color: "white" }}>{` ${index + 1} / 5`}</div>
        { index < 4 && <button id="prev" onClick={this.handleClick}>See Previous Day</button> }
        { index > 0 && <button id="next" onClick={this.handleClick}>See Next Day</button> }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    APODS: getAPODS(state),
  }
}

const mapDispatchToProps = {
  getAPOD,
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotoOfTheDay);
