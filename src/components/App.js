import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Particles from 'react-particles-js';
import particleParams from '../lib/particleParams';
import PhotoOfTheDay from '../components/PhotoOfTheDay';

import './../stylesheets/App.css';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Helmet title="Nasa APOD" />
        <PhotoOfTheDay />
        <Particles className="particle" params={particleParams} />
      </div>
    );
  }
}
