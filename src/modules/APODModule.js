import { createSelector } from 'reselect'

// Selectors
const getAPODSState = state => state.APODS;

export const getAPODS = createSelector(
  [getAPODSState],
  APODS => APODS,
);

// Actions
export const APOD_GET = 'APOD_GET';
export const APOD_SET = 'APOD_SET';

export function getAPOD(unit) {
  return { type: APOD_GET, unit }
}

export function setAPOD(payload) {
  return { type: APOD_SET, payload }
}

const initState = [];
export default function APODSModule(state = initState, action) {
    switch (action.type) {
      case APOD_SET:
        return [
          ...state,
          action.payload,
        ]
      default:
        return state
    }
  }